﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShip : MonoBehaviour {

    public float degrees;
    public float m_MaxSpeed;
    public float m_Acceleration;
    public GameObject m_Pivot;
    public float m_SlowDownVelocity;
    public Vector2 m_Velocity;

    public GameObject m_Projectile;
    // Use this for initialization
    void Start () {
    }

	private void ShipRotation()
    {
        if (Input.GetKey(KeyCode.A))
            gameObject.transform.RotateAround(m_Pivot.transform.position, gameObject.transform.forward, degrees);
        if (Input.GetKey(KeyCode.D))
            gameObject.transform.RotateAround(m_Pivot.transform.position, -gameObject.transform.forward, degrees);
    }

    private void ShipThruster()
    {

        if (Input.GetKey(KeyCode.W))
        {
            Vector3 facingVector = gameObject.transform.up * m_Acceleration;
            m_Velocity += (Vector2)facingVector;
            if (m_Velocity.magnitude > m_MaxSpeed)
            {
                m_Velocity.Normalize();
                m_Velocity.Scale(new Vector2(m_MaxSpeed, m_MaxSpeed));
            }
        }
        else
        {
            //Debug.Log(m_Velocity.magnitude);
            if (m_Velocity.magnitude > 0)
                m_Velocity = ReduceLength(m_Velocity, m_SlowDownVelocity);
        }

    }

    private void WallCheck()
    {
        if (gameObject.transform.position.x >= 13)
            gameObject.transform.position = new Vector3(-13, gameObject.transform.position.y);
        else if (gameObject.transform.position.x <= -13)
            gameObject.transform.position = new Vector3(13, gameObject.transform.position.y);
        if (gameObject.transform.position.y >= 7)
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, -4);
        else if (gameObject.transform.position.y <= -4)
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 7);
    }
    // Update is called once per frame
    void Update ()
    {
        ShipRotation();
        ShipThruster();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }

        gameObject.transform.position += new Vector3(m_Velocity.x, m_Velocity.y) * Time.deltaTime;

        WallCheck();

    }

    private void Shoot()
    {
        GameObject Projectile = 
             Instantiate(m_Projectile, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y), Quaternion.identity);
        
        Projectile.GetComponent<ProjectileBehavior>().PostStart(gameObject.transform.up);

        //Projectile.transform.parent = gameObject.transform;

    }

    private Vector2 ReduceLength(Vector2 originalVector, float reducedAmmout)
    {
        Vector2 Final = originalVector;

        Final = Final * (1 - reducedAmmout / originalVector.magnitude);

        return Final;
    }

}
