﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour {

    public GameObject m_AsternoidManager;

    private void Awake()
    {
        if (m_AsternoidManager == null)
            Instantiate(m_AsternoidManager);
    }
}
