﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsternoidManager : MonoBehaviour {
    public GameObject m_AsternoidPrefab;
    public float m_SpawnTimeInterval;
    private float m_TempSpawnTime;
    public List<GameObject> m_Asternoids;

    public static AsternoidManager m_Instance = null;

    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else if (m_Instance != null)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(m_Instance.m_TempSpawnTime > m_Instance.m_SpawnTimeInterval)
        {
            m_Instance.m_TempSpawnTime = 0;
            if(m_Instance.m_Asternoids.Count < 10)
                m_Instance.m_Asternoids.Add(Instantiate(m_AsternoidPrefab));
        }
        m_Instance.m_TempSpawnTime += Time.deltaTime;

    }
}
